import http from 'k6/http';
import {check, sleep} from 'k6';
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";

export function handleSummary(data) {
    return {
      "stress_test.html": htmlReport(data),
    };
  }

export let options ={
    stages: [
        {duration: '10s', target: 50},
        {duration: '10s', target: 40},
        {duration: '5s', target: 50},
        {duration: '5s', target: 60},
        {duration: '10s', target: 40},
    ],
    thresholds: {
        http_req_duration: ['p(95)<500'],
        http_req_failed: ['rate<0.1']
    }
};

const headers = {'Content-Type': 'application/json'};

export default function () {
    const res = http.get('https://run.mocky.io/v3/1b6d9480-44c4-49ae-a095-27e780edf0eb')

    check(res, {
        'Response returns 200': (r) => r.status == 200
    });
}